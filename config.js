/*
 * This file contains javascript objects that configure the program
 */
module.exports = {
   SERVER :                           // Info about where application is running
   {
      baseURL:   'https://attendance-application.herokuapp.com',
      UUIDEndpoint:  '/api/basestation/{id}/newUUID',
      port: 0
   },
   LOCATION:   'AER135',              // Physical location of a base_station
   NAME:       'AER 135',             // Name this basestation brodcasts
   UUID:       'ec00',                // The default UUID
   ID:         2
}
