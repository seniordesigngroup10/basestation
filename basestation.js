var config = require('./config')
var bleno = require('bleno')
var rp = require('request-promise')
var winston = require('winston')
var url = (config.SERVER.baseURL +
    config.SERVER.UUIDEndpoint)

winston.add(winston.transports.File, {filename:'basestation.log'})

//winston.level = 'error'
bleno.on('stateChange', function(state) {
   winston.log('info','on -> stateChange: ' + state)
   
   if (state === 'poweredOn'){

      winston.log('info', 'getting UUID from server')
      rp({url: url.replace('{id}',config.ID)})
       .then( function (body) {
          winston.log('debug', JSON.parse(body))
          config.UUID = JSON.parse(body).uuid
          winston.log('info','New UUID::' + config.UUID)

       })
       .catch(function(err) {
          winston.log('error', err)
          winston.log('info', 'Keeping old UUID')
       }).finally(function(){
          winston.log('info','Starting Bluetooth')
          bleno.startAdvertising(config.NAME, [config.UUID])
       })
      
   } else {
      bleno.stopAdvertising()
   }
})

process.stdin.resume();//so the program will not close instantly

function exitHandler(options, err) {
   if (options.cleanup){
      winston.log('info', 'natrually exiting program')
   }else if (options.exit){
      winston.log('info', 'recived [SIGINT], exiting program')
   }else if (err){
      winston.log('error',err)
   }
   bleno.stopAdvertising()
   process.exit();
}



process.on('exit', exitHandler.bind(null,{cleanup:true}));
process.on('SIGINT', exitHandler.bind(null, {exit:true}));
process.on('uncaughtException', exitHandler.bind(null, {exit:true}));
