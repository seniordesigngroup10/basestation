![@tend](https://bitbucket.org/seniordesigngroup10/basestation/raw/master/newnew.png)

# base station #

Contains all code for the base station used by Senior Design (class of 2017) Group 10 at WVU to track attendance of students.

Group Members: 

-  Benjamin Waechter (benjaminwaechterm@gmail.com)
-  Timothy Reardon


## System Requirements ##

### Hardware ###
When this was developed, a [Raspberry Pi 3 model B](https://www.raspberrypi.org/products/raspberry-pi-3-model-b/) was used for testing. The only Hardware requirement is a computer with a Bluetooth card that supports BLE (4.0).

### Software ###

Durring development the hardware was running [arch linux](https://www.archlinux.org/) as our linux distribution to eliminate unneeded fluff.  Any Linux distribution (or GNU/Linux or for are a Stallmanite) can be used.

The system is a Linux service using systemd as its implementation, tough could easily be modified to work in another way.  The base-station can also be run standalone(not as a service).

The following major software packages are required to use the base station software:

- [nodejs](https://nodejs.org/en/) - the de-facto desktop JavaScript interperator
- [npm](https://www.npmjs.com/) - a package manager for node
- [BlueZ](http://www.bluez.org/) - The Bluetooth Stack for Linux 

Once all reqirements are installed and the repository is cloned(clone into home directory is reccomended):
```bash
cd basestation
npm install      # Installs all packages listed as dependencies in the package.json file
npm -g bleno     # Install the bleno dependency globally to avoid permission issues
```

To run the basestation simply `node basestation`.  If you wish to use this as a service you can configure your local systemd to use the `basestation.service` provided with the code.  For more on this please read up on [systemd](https://www.freedesktop.org/wiki/Software/systemd/) and [systemctl](https://www.freedesktop.org/software/systemd/man/systemctl.html)  (the man pages are your friend).

## Future ##

We hope to pass this project onto a future group as it has real potential.  This is close to something great, just needs a bit of refinement.  If you go to WVU and wish to use this a a project further please contact Benjamin Waechter at the email address listed above.